# The Knapsack Problem #

**A dynamic program-based solution**

Given an input as in [cases/1](./cases/1), as follows,

```
25
8
2 8 4 9 1 4 8 5
1.5 3.49 2.8 1.14 8.32 2.8 4.72 1.92
```

which is explained as: *a)* the first line has one
*integer* `max_size`; *b)* the second line has one
*integer* `num_items`; *c)* the third line is a
`NUM_ITEMS` long list of *space-separated integers*
representing `size` of each item; and *d)* the fourth
line is `NUM_ITEMS` long list of *space-separated real
numbers* representing the `value` of each item. 


The program takes input from `STDIN`, computes the
configuration of a knapsack limited by `MAX_SIZE`, with
maximum possible `VALUE`, and writes to `STDOUT`;
something like:

```
main: max value: 22.13
main: selected (item, size, value): (1, 8, 3.49) (2, 4, 2.8) (3, 9, 1.14) (4, 1, 8.32) (5, 4, 2.8) (6, 8, 4.72) 
```

## Build/Usage ##

```sh
PREFIX=knapsack
INSTALL_PREFIX=${PREFIX}/build/install
git clone 'https://gitlab.com/goaldus/knapsack.git' ${PREFIX}
mkdir -p ${PREFIX}/build
cd ${PREFIX}/build
cmake .. -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} \
    && make                                       \
    && make install
cd ..

${INSTALL_PREFIX}/bin/RunKnapsack
```

## Memoization ##

This code uses a generic `memoize` function [from
here](https://www.reddit.com/r/cpp/comments/12zw1u/memoization_in_c11/c6zmlxu/).

Other options for memoization (not tried):
1. [a (thread safe) C++17 function template that acts
   like std::invoke but memoizes the
   result](https://stackoverflow.com/a/60903898) - from
   SO. **not tested**
2. There are more, just search for *&#8220;c++ memoization&#8221;*.


## Pretty Printing for C++ ##

This is a third party header only library, that we use:

[`cxx-prettyprint`: A C++ Container Pretty-Printer](http://louisdx.github.com/cxx-prettyprint/)
