#include <iostream>
#include <tuple>
#include <map>
#include <type_traits>
#include <functional>

#include <prettyprint.hpp>

namespace knapsack{
  template<class... I, class O>
  function<O(I...)> memoize(function<O(I...)> f) {
    typedef map<std::tuple<typename std::decay<I>::type...>, O> memo_t;
    static memo_t memos;

    return [=](I... i) mutable -> O {
      auto args(std::tie(i...));
      auto it(memos.lower_bound(args));

      if (it == memos.end() || it->first != args) {
        it = memos.insert(it, make_pair(args, f(i...)));
        std::cerr << "sep_memo::memoize: " << it->first
             << " -> " << it->second << endl;
      } else {
        std::cerr << "sep_memo::memoize: recall "
                  << it->first << endl;
      }

      return it->second;
    };
  }
}
