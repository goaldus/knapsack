#include <unordered_map>
#include <functional>
#include <utility>
#include <vector>

#include <boost/container_hash/hash.hpp>

namespace knapsack {

  typedef std::uint32_t           sz_t;
  typedef std::uint16_t           item_t;
  typedef float                   val_t;
  typedef std::vector<val_t>      Values;
  typedef std::vector<sz_t>       Sizes;
  typedef std::vector<bool>       Mask;
  typedef std::pair<item_t, sz_t> SubProblem;
  typedef std::unordered_map<
    SubProblem, val_t, boost::hash<SubProblem>
    >                             Lut;

}
