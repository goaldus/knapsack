#include <knapsack/types.hpp>

std::tuple<knapsack::sz_t, knapsack::Sizes, knapsack::Values>
parse_input(std::istream& strm);
