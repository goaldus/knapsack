#include <vector>
#include <knapsack/types.hpp>

namespace knapsack {

  struct Knapsack {
    sz_t max_size;
    Sizes sizes;
    Values values;
    Mask used;

    Knapsack(
      sz_t max_size, Sizes sizes, Values values
      ): max_size(max_size), sizes(sizes),
         values(values), used(Mask(values.size(), false))
      {}

    val_t solve();
    val_t reducer(item_t i, sz_t X);
    item_t next(item_t i);
    bool is_valid_item(item_t i);

  };

}  // knapsack
