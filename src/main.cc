#include <iostream>
#include <tuple>
using namespace std;

#include <boost/range/irange.hpp>
using boost::irange;

#include <knapsack/types.hpp>
#include <knapsack/parse_input.hpp>
#include <knapsack/memoize.hpp>
#include <knapsack/knapsack.hpp>
using namespace knapsack;

int main(int argc, char *argv[])
{
  sz_t max_size;
  Sizes item_sizes;
  Values item_values;

  std::tie(
    max_size, item_sizes, item_values
    ) = parse_input(std::cin);

  auto ks = Knapsack(max_size, item_sizes, item_values);
  auto max_value = ks.solve();
  cout << "main: max value: " << max_value << endl;

  cout << "main: selected (item, size, value): ";
  for ( auto i : irange(item_sizes.size())) {
    if (ks.used[i])
      cout << "(" << i
           << ", " << item_sizes[i]
           << ", " << item_values[i]
           <<  ") ";
  }
  cout << endl;

  return 0;
}
