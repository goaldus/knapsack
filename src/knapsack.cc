#include <functional>
using namespace std;

#include <knapsack/types.hpp>
#include <knapsack/knapsack.hpp>
#include <knapsack/memoize.hpp>

namespace knapsack {

  val_t Knapsack::solve() {
    using std::placeholders::_1;
    using std::placeholders::_2;
    auto solver = memoize(
      function<val_t(item_t, sz_t)>(
        bind(&Knapsack::reducer, this, _1, _2)
        ));

    return solver(0, max_size);
  }

  val_t Knapsack::reducer(item_t i, sz_t X) {
    using std::placeholders::_1;
    using std::placeholders::_2;
    auto recurse = memoize(
      function<val_t(item_t, sz_t)>(
        bind(&Knapsack::reducer, this, _1, _2)
        ));

    val_t result = 0;

    auto j = this->next(i);
    if (this->is_valid_item(j)) {
      auto s_i = this->sizes[i];

      auto with_item = (X < s_i) ? 0 : (
        recurse(j, X - s_i) + this->values[i]
        );

      auto without_item = recurse(j, X);

      if (without_item > with_item) {
        result = without_item;
      } else {
        result = with_item;
        used[i] = true;
      }
    }

    return result;
  }

  item_t Knapsack::next(item_t i) {
    return 1+i;
  }

  bool Knapsack::is_valid_item(item_t i) {
    return (i >= 0 && i < sizes.size());
  }

}  // knapsack
