#include <iostream>
#include <tuple>
#include <knapsack/types.hpp>

#include <boost/range/irange.hpp>

using namespace knapsack;


std::tuple<sz_t, Sizes, Values>
parse_input(std::istream& strm) {
  using boost::irange;

  sz_t max_size, sz;
  item_t num_items;
  val_t val;
  auto item_sizes = Sizes ();
  auto item_values = Values ();

  strm >> max_size;
  strm >> num_items;

  for (auto _ : irange(num_items)) {
    strm >> sz;
    item_sizes.push_back(sz);
  }

  for (auto _ : irange(num_items)) {
    strm >> val;
    item_values.push_back(val);
  }

  return std::make_tuple(
    max_size, item_sizes, item_values
    );
}
