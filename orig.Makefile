BIN := bin
SRC := main.cc parse_input.cc knapsack.cc
TESTS := $(wildcard test_*.cc)
SAMPLES := $(wildcard sample_*.cc)
HEADERS := $(wildcard *.hpp)

all : ${BIN} binary tests samples

binary : ${BIN}/knapsack
${BIN}/knapsack : ${SRC} ${HEADERS}
	g++ -o $@ $(filter %.cc,$^)

tests : ${TESTS:test_%.cc=${BIN}/test_%}
${BIN}/test_% : test_%.cc %.cc ${HEADERS}
	g++ -o $@ $(filter %.cc,$^)

samples: ${SAMPLES:sample_%.cc=${BIN}/sample_%}
${BIN}/sample_% : sample_%.cc ${HEADERS}
	g++ -o $@ $(filter %.cc,$^)

${BIN} :
	[ -d $@ ] || mkdir -p ${BIN}
