#include <iostream>
#include <iomanip>
#include <map>
#include <functional>
using namespace std;

#include <boost/range/irange.hpp>
using boost::irange;

typedef unsigned long long ull;

namespace coupled_memo {
  ull fib(ull n) {
    typedef std::map<ull, ull> memo_t;
    static memo_t memo;

    auto _fib = [](ull k) {
      return (k < 2) ? k : (
        fib(k-1) + fib(k-2)
        );
    };

    auto loc = memo.lower_bound(n);
    if (loc == memo.end() or loc->first != n) {
      loc = memo.insert(loc, make_pair(n, _fib(n)));
      cerr << "coupled_memo::fib: (memoize): " << loc->first
           << " -> " << loc->second << endl;
    }

    return loc->second;
  }
}

namespace sep_memo {
  template<class... I, class O>
  function<O(I...)> memoize(function<O(I...)> f) {
    typedef map<std::tuple<typename std::decay<I>::type...>, O> memo_t;
    static memo_t memos;

    return [=](I... i) mutable -> O {
      auto args(std::tie(i...));
      auto it(memos.lower_bound(args));

      if (it == memos.end() || it->first != args) {
        it = memos.insert(it, make_pair(args, f(i...)));
        cerr << "sep_memo::memoize: " << get<0>(it->first)
             << " -> " << it->second << endl;
      }

      return it->second;
    };
  }

  struct _Fib {
    ull operator()(ull n) {
      auto recurse = memoize(function<ull(ull)>(_Fib()));
      return (n < 2) ? n : (
        recurse(n-1) + recurse(n-2)
        );
    }
  };

  ull fib(ull n) {
    auto f = memoize(function<ull(ull)>(_Fib()));
    return f(n);
  }
}

int main(int argc, char *argv[])
{
  for (auto i : irange(25)) {
    auto f = coupled_memo::fib(i);
    cout << setw(2) << i << ": " << f << endl;
  }

  cout << endl << endl;

  for (auto i : irange(25)) {
    auto f = sep_memo::fib(i);
    cout << setw(2) << i << ": " << f << endl;
  }

  return 0;
}
