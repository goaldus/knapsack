#include <iostream>
#include <knapsack/types.hpp>
#include <knapsack/parse_input.hpp>
#include <tuple>

using namespace std;
using namespace knapsack;

int main(int argc, char *argv[])
{
  sz_t max_size;
  Sizes item_sizes;
  Values item_values;

  std::tie(
    max_size, item_sizes, item_values
    ) = parse_input(std::cin);

  cout << max_size;
  cout << endl;

  for (auto sz : item_sizes) cout << sz << ' ';
  cout << endl;
  
  for (auto val : item_values) cout << val << ' ';
  cout << endl;

  return 0;
}
