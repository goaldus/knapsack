#include <iostream>

#include <knapsack/types.hpp>

using namespace std;
using namespace knapsack;

int main(int argc, char *argv[])
{
  auto lut = Lut();
  auto sp = SubProblem(4, 73);
  lut[sp] = 8.5;
  cout << lut[sp] << endl;
  return 0;
}
