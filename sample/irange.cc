#include <iostream>
#include <boost/range/irange.hpp>

using namespace boost;
using namespace std;

int main(int argc, char *argv[])
{
  for (auto i : boost::irange(5)) {
    cout << i << ' ';
  }
  
  cout << endl;

  return 0;
}
